#(ord('P') - ((ord('P') - ord('a')) % 256 ))%256
import compressor
import decompressor
import sys,getopt
import filecmp
import zipfile

###############################################################################
def main(argv):
	inputfile = ''
	outputfile = ''

	compressorMode = False
	decompressorMode = False
	debugMode = False

	try:
		opts, args = getopt.getopt(argv,"hi:o:CUD",["ifile=","ofile="])
	except getopt.GetoptError:
		print ('test.py -i <inputfile> -o <outputfile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print ('Usage: codec.py -[C(ompress)|U(ncompress)|D(ebug)] -i <inputfile> -o <outputfile>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfile = arg
		elif opt == '-C':
			compressorMode = True
		elif opt == '-U':
			decompressorMode = True
		elif opt == '-D':
			debugMode = True

	print ('Input file is "', inputfile)
	print ('Output file is "', outputfile)


	if compressorMode:
		rawFile = open(inputfile, 'rb')
		data = rawFile.read()
		predictors, residues = compressor.ENCODE(data)
	elif decompressorMode:
		recoveredData = decompressor.DECODE(inputfile)#'outputFiles/myzip.zip')
		recoveredFile = open('outputFiles/recovered.txt','wb')
		recoveredFile.write(bytearray(recoveredData.encode()))
		recoveredFile.close()
	elif debugMode:
		rawFile = open(inputfile, 'rb')
		data = rawFile.read()
		predictors, residues = compressor.ENCODE(data)
		recoveredData = decompressor.DECODE('outputFiles/myzip.zip')
		recoveredFile = open('outputFiles/recovered.txt','wb')
		recoveredFile.write(bytearray(recoveredData))
		recoveredFile.close()

		#Create compressed zip archive and add files
		z = zipfile.ZipFile("outputFiles/theirzip.zip", "w",zipfile.ZIP_DEFLATED)
		z.write(inputfile)
		z.printdir()
		z.close()

		if(filecmp.cmp(inputfile, 'outputFiles/recovered.txt')):
			print ("================== SUCCESS ==================")
		else:
			print (".................. FAILED ...................")

if __name__ == "__main__":
   main(sys.argv[1:])