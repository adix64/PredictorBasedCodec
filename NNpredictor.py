import tensorflow as tf
import numpy
import csv
_INFINITY = 999999
# import matplotlib.pyplot as plt
rng = numpy.random

# ML Parameters
examples = []
learning_rate = 0.01
training_epochs = 50
display_step = 50
gFixedWeights, gFixedBiases = None, None
gInput = None
gRuntimeTFsession = None
gPredictedOutput = None

def char2alphaEnc(ch):
	ret = [0.0 for i in range(256)]
	ret[ord(ch)] = 1.0
	return ret

def str2alphaEnc(pstr):
	ret = []
	for i in range(len(pstr)):
		ret = ret + char2alphaEnc(pstr[i])
	return ret

def TrainNN(fileName, k):
	rawFile = open(fileName, 'rb')
	data = rawFile.read().decode("ascii")
	for i in range(0, len(data) - k - 1):
		# print(data[i : i + k])
		xFeed = str2alphaEnc(data[i : i + k])
		yFeed = char2alphaEnc(data[i + k])
		xFeed = [[xFeed[i]] for i in range(len(xFeed))]
		yFeed = [[yFeed[i]] for i in range(len(yFeed))]
		examples.append((xFeed, yFeed))

	# tf Graph Input
	X = tf.placeholder(tf.float32, shape = (k * 256, 1))
	Y = tf.placeholder(tf.float32, shape = (256, 1))

	# Set model weights
	W = tf.Variable([[rng.randn() for j in range(k * 256)]for i in range(256)], name="weight")
	b = tf.Variable([[rng.randn()] for i in range(256)], name="bias")

	# Construct a linear model
	pred = tf.add(tf.matmul(W, X), b)

	# Mean squared error
	cost = tf.reduce_sum(tf.pow(pred-Y, 2))
	# Gradient descent
	#  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
	optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

	# Initialize the variables (i.e. assign their default value)
	init = tf.global_variables_initializer()

	# Start training
	with tf.Session() as sess:

		# Run the initializer
		sess.run(init)

		# Fit all training data
		for epoch in range(training_epochs):
			print("Training the NN - Epoch %d" % epoch)
			for (xFeed, yFeed) in examples:
				sess.run(optimizer, feed_dict={X: xFeed, Y: yFeed })

		print("Training DONE.")
		
		W_val, b_val = sess.run([W, b])

		numpy.savetxt("W.csv", W_val, delimiter=",")
		numpy.savetxt("b.csv", b_val, delimiter=",")


def readFloats(fileName):
	# result = numpy.loadtxt(open(fname, "rb"), delimiter=",")#, skiprows=1)
	rawFile = open(fileName, 'rt')
	data = rawFile.read()
	data = data.replace('\n', ',')
	data = data.replace('\r', ',')
	# print(result)
	# return result

	return numpy.fromstring(data, dtype=float, sep = ',')

def NNinitWeightsBiasesFromFile(weightsFile, biasesFile, k):
	global gFixedWeights, gFixedBiases
	global gInput, gPredictedOutput
	global gRuntimeTFsession
	
	linWeights = readFloats(weightsFile)
	linBiases = readFloats(biasesFile)
	cnt = 0
	shapedWeights = []

	for i in range(256):
		oneline = []
		for j in range(k * 256):
			oneline.append(linWeights[cnt])
			cnt += 1
		shapedWeights.append(oneline)

	shapedBiases = [[linBiases[i]] for i in range(256)]
	#Set input shape
	gInput = tf.placeholder(tf.float32, shape = (k * 256, 1))
	# Set model weights
	gFixedWeights = tf.Variable(shapedWeights, name="fixedWeight")
	gFixedBiases = tf.Variable(shapedBiases, name="fixedBias")

	# Construct a linear model
	gPredictedOutput = tf.add(tf.matmul(gFixedWeights, gInput), gFixedBiases)

	init = tf.global_variables_initializer()
	# Run the initializer
	gRuntimeTFsession = tf.Session()
	gRuntimeTFsession.run(init)
		
def NNpredictNextCharacter(history, k):
	global gFixedWeights, gFixedBiases
	global gInput, gPredictedOutput
	global gRuntimeTFsession

	if len(history) < k:
		return '\0'

	xFeed = str2alphaEnc(history[-k:])
	xFeed = [[xFeed[i]] for i in range(len(xFeed))]
	result = gRuntimeTFsession.run(gPredictedOutput, feed_dict = {gInput : xFeed})

	maxS = -_INFINITY
	besti = -1
	i = 0
	for r in result:
		if r[0] > maxS:
			maxS = r[0]
			besti = i
		i += 1
	# print("And the winner is... ")
	return chr(besti)

# TrainNN("inputFiles/trainData.txt", 4)