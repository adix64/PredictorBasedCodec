import predictors
import zipfile
from subprocess import call
def _DECODE(INpredictors, residues):
	recoveredData = ''
	residuesIdx = 0

	for p in INpredictors:
		# print "------------------------------------\nRebuilding with %s, rIdx [%d]....: %s" % (p,residuesIdx, recoveredData)

		if 'LAST' in p:
			recoveredData = recoveredData + recoveredData[-predictors.TP[p][1]:]
		elif 'NONE' in p:
			recoveredData = recoveredData + "".join(residues[residuesIdx:residuesIdx + predictors.TP[p][1]])
			residuesIdx += predictors.TP[p][1]
		else:
			predicted = predictors.TP[p][0](recoveredData, len(recoveredData))##????????NONE HANDLE
			residue = residues[residuesIdx:residuesIdx + predictors.TP[p][1]]	
			recoveredChunk = ''
			for i in range(len(residue)):
				recoveredChunk = recoveredChunk + chr((ord(predicted[i]) - ord(residue[i])) % 256)
			recoveredData = recoveredData + recoveredChunk
			residuesIdx += predictors.TP[p][1]

	# print "======================================================="
	# print "=========================DONE=========================="
	# print "======================================================="
	# print "===================RECOVERED DATA:=====================\n"
	# print recoveredData
	# print "\n======================================================="
	# print "======================================================="
	# print ">>>>>>>>>>>>>>>> PREDICTORS >>>>>>>>>>>>>>>>" 
	# print INpredictors
	# print ">>>>>>>>>>>>>>>> RESIDUES >>>>>>>>>>>>>>>>>>"

	# print residues
	# print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
	return recoveredData

def DECODE(archiveName):
	predictors.InitTableOfPredictors()
	z = zipfile.ZipFile(archiveName, "r",zipfile.ZIP_DEFLATED)
	z.extractall(".")
	z.close()

	predFile = open('PREDICTORS.txt','rb')
	resFile = open('RESIDUES.txt','rb')
	predictorsBytes = predFile.read().decode("utf-8")
	residues = resFile.read().decode("utf-8")
	predFile.close()
	resFile.close()
	call(["rm", "-f", "PREDICTORS.txt"])
	call(["rm", "-f", "RESIDUES.txt"])
	INpredictors = [predictors.byte2PredMap[predictorsBytes[i]] for i in range(len(predictorsBytes))]
	return _DECODE(INpredictors, residues)