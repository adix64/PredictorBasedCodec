import NNpredictor

TP = {}
pred2ByteMap = {}
byte2PredMap = {}


MAX_NN_HISTORY_BYTES = 4

def nnPred(data, ptr):
	return NNpredictor.NNpredictNextCharacter(data[ptr-MAX_NN_HISTORY_BYTES:ptr], MAX_NN_HISTORY_BYTES)

def last8(data, ptr):
	return data[ptr-1:ptr]
def last16(data, ptr):
	return data[ptr-2:ptr]
def last32(data, ptr):
	return data[ptr-4:ptr]
def last64(data, ptr):
	return data[ptr-8:ptr]
def last128(data, ptr):
	return data[ptr-16:ptr]


def none8(data, ptr):
	return data[ptr:ptr+1]
def none16(data, ptr):
	return data[ptr:ptr+2]
def none32(data, ptr):
	return data[ptr:ptr+4]
def none64(data, ptr):
	return data[ptr:ptr+8]
def none128(data, ptr):
	return data[ptr:ptr+16]


def lerp8(data, ptr):
	t = 0.5
	return (1 - t) * data[ptr-1] + t * data[ptr]
def lerp16(data, ptr):
	t = 0.5
	return (1 - t) * data[ptr-2] + t * data[ptr]
def lerp32(data, ptr):
	t = 0.5
	return (1 - t) * data[ptr-4] + t * data[ptr]
def lerp64(data, ptr):
	t = 0.5
	return (1 - t) * data[ptr-8] + t * data[ptr]
def lerp128(data, ptr):
	t = 0.5
	return (1 - t) * data[ptr-16] + t * data[ptr]


def diff8(data, ptr):
	ret = ''
	for i in data[ptr-1:ptr]:
		ret = ret + chr(255-ord(i))
	return ret

def diff16(data, ptr):
	ret = ''
	for i in data[ptr-2:ptr]:
		ret = ret + chr(255-ord(i))
	return ret

def diff32(data, ptr):
	ret = ''
	for i in data[ptr-4:ptr]:
		ret = ret + chr(255-ord(i))
	return ret
def diff64(data, ptr):
	ret = ''
	for i in data[ptr-8:ptr]:
		ret = ret + chr(255-ord(i))
	return ret

def diff128(data, ptr):
	ret = ''
	for i in data[ptr-16:ptr]:
		ret = ret + chr(255-ord(i))
	return ret

def InitTableOfPredictors():
	global TP, pred2ByteMap, byte2PredMap

	
	TP = {}
	TP['LAST8'] = (last8, 1)
	TP['LAST16'] = (last16, 2)
	TP['LAST32'] = (last32, 4)
	TP['LAST64'] = (last64, 8)
	TP['LAST128'] = (last128, 16)

	# TP['LERP8'] 	= (lerp8  , 1 )
	# TP['LERP16'] 	= (lerp16 , 2 )
	# TP['LERP32'] 	= (lerp32 , 4 )
	# TP['LERP64'] 	= (lerp64 , 8 )
	# TP['LERP128'] 	= (lerp128, 16)

	TP['NONE8'] = (none8, 1)
	TP['NONE16'] = (none16, 2)
	TP['NONE32'] = (none32, 4)
	TP['NONE64'] = (none64, 8)
	TP['NONE128'] = (none128, 16)

	TP['DIFF8'] = (diff8, 1)
	TP['DIFF16'] = (diff16, 2)
	TP['DIFF32'] = (diff32, 4)
	TP['DIFF64'] = (diff64, 8)

	NNpredictor.NNinitWeightsBiasesFromFile("W.csv", "b.csv", MAX_NN_HISTORY_BYTES)
	TP['NN'] = (nnPred, 1)


	i = 0
	for pred in TP:
		pred2ByteMap[pred] = chr(i)
		byte2PredMap[chr(i)] = pred
		i+=1
	# TP['NEG8'] = (neg8, 1)
	# TP['NEG16'] = (neg16, 2)
	# TP['NEG32'] = (neg32, 4)
	# TP['NEG64'] = (neg64, 8)
