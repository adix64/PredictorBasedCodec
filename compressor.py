import predictors
import zipfile
from copy import deepcopy
from subprocess import call
#GLOBAL VARIABLES
NBRU = 3
OUTpredictors = []
OUTresidues = []
TRUP = {'NIT': None}
predResidues = {}
data = []
ptr = -1
TP = {}
###############################################################################

def CompressionSysInit():
	global OUTpredictors, OUTresidues
	global TP, TRUP, predResidues
	global data, ptr 
	OUTpredictors = []
	OUTresidues = []
	predictors.InitTableOfPredictors()
	TP = predictors.TP
	TRUP = {'NIT': None}
	predResidues = {}
	data = []
	ptr = -1

###############################################################################

def Redundancy(predictorName):
	global OUTpredictors, OUTresidues
	global TP, TRUP, predResidues
	global data, ptr 

	predictor = TP[predictorName]

	numGuessedBytes = 0.0
	predicted = predictor[0](data,ptr)
	actualData = data[ptr:ptr+predictor[1]]
	if 'LAST' in predictorName:
		if predicted != actualData:
			return -999.0
		else:
			return predictor[1] * 99

	residue = ''
	# print TRUP
	# print "PREDICTOR: " + predictorName
	# print "PREDICTED:" + predicted + " ----  ACTUAL DATA:" + actualData
	for i in range(len(actualData)):
		diff = ord(predicted[i]) - ord(actualData[i])
		residue = residue + chr(diff % 256)
		# am ghicit byte-ul i(+1)sau nu (-1,penalizare)
		numGuessedBytes +=   1.0 if (diff == 0) else 0.0
	predResidues[predictorName] = residue

	numDifferences = 1.0 if (residue[0] != OUTresidues[-1]) else 0.0
	for i in range(len(residue) - 1):
		numDifferences += 1.0 if (residue[i] != residue[i+1]) else 0.0
	
	if 'NN' in predictorName:
		numGuessedBytes *= 100

	return numGuessedBytes# + numDifferences * numDifferences

###############################################################################

def GetPredWithMaxRedundancy(predTable):
	global OUTpredictors, OUTresidues
	global TP, TRUP, predResidues
	global data, ptr 

	maxRedundancy = -999999999.0
	bestPred = None
	for predictor in predTable:
		redun = Redundancy(predictor)
		# print("PREDICTOR : %s ... SCORE: %d" % (predictor, redun))
		# print "Pdctr:[%10s] Rdcy:[%4.2f]" % (predictor, redun)
		if redun > maxRedundancy:
			bestPred = predictor
			maxRedundancy = redun
	# print "Winner Pdctr: %s, Score: %d" %(bestPred,maxRedundancy)
	return maxRedundancy, bestPred

###############################################################################
THRESHOLD = 1
def ENCODE(inputData):
	print ("Encoding... please wait.")
	global OUTpredictors, OUTresidues
	global TP, TRUP, predResidues
	global data, ptr

	CompressionSysInit()
	
	data = inputData.decode("ascii")
	# print data

	dataSZ = len(data)
	MRUP = 'NIT'
	NITflag = False
	#while ptr < dataSZ and len(TRUP) < 2 ** NBRU:
		# #populam TRUP......................
	ptr = 16
	OUTpredictors = ['NONE128']
	OUTresidues = data[0:16]
	iteratie = 0
	# print "DATA SiiiiiiiiiiiiiiiiiiiiiiiiiiIIZeeeeeeeeeeeeeeeee : %d"%dataSZ
	while(ptr < dataSZ): #parcurgem si codificam fisierul
		iteratie+=1
		# print "PASS[%d]_______________________________ptr=[%d]"%(iteratie, ptr)
		if NITflag or len(TRUP) < 2 ** NBRU: 
			maxRedundancy, predictor = GetPredWithMaxRedundancy(TP)

			if NITflag:
				del TRUP[MRUP]

			MRUP = predictor
			TRUP[predictor] = TP[predictor] 
			OUTpredictors.append(predictor)

			if('NONE' in predictor):
				OUTresidues = OUTresidues + data[ptr:ptr+TP[predictor][1]]
					# print "APPENDING RESIDUE-0-...: %s" % predResidues[predictor]
			elif 'LAST' not in predictor:
				OUTresidues = OUTresidues + predResidues[predictor]
				# print "APPENDING RESIDUE-1-...: %s" % predResidues[predictor] 
			ptr = ptr + TP[predictor][1]
			# print "TP[predictor][1] %d"  % TP[predictor][1] 
		else:
			maxRedundancy, predictor = GetPredWithMaxRedundancy(TRUP)
			if (maxRedundancy > THRESHOLD):
				# print "THRESHOOOOLD %d" % THRESHOLD
				del TRUP[MRUP]
				MRUP = predictor
				TRUP[predictor] = TP[predictor] 
				OUTpredictors.append(predictor)
				if('NONE' in predictor):
					OUTresidues = OUTresidues + data[ptr:ptr+TP[predictor][1]]
					# print "APPENDING RESIDUE-2-...: %s" % predResidues[predictor]
				elif 'LAST' not in predictor:
					OUTresidues = OUTresidues + predResidues[predictor]
					# print "APPENDING RESIDUE-3-...: %s" % predResidues[predictor] 
				ptr = ptr + TP[predictor][1]
				# print "TP[predictor][1] %d" % TP[predictor][1]
			else:
				NITflag = True
				# print "___________!=== CALL N.I.T. ===!____________"
				continue
		NITflag = False
	NNwasRight = 0.0
	for prd in OUTpredictors:
		if'NN' in prd:
			NNwasRight += 1.0
	print("NN was right %f of times" % (NNwasRight / len(OUTpredictors) * 100.0))
	# print(OUTpredictors)
	# print predictors.pred2ByteMap
	predictorFileStr = "".join([predictors.pred2ByteMap[OUTpredictors[i]] for i in range(len(OUTpredictors))])
	predictorsFile = open('PREDICTORS.txt','wb')
	residuesFile = open('RESIDUES.txt','wb')
	predictorsFile.write(bytearray(predictorFileStr.encode()))
	residuesFile.write(bytearray(OUTresidues.encode()))
	predictorsFile.close()
	residuesFile.close()
	#Create compressed zip archive and add files
	z = zipfile.ZipFile("outputFiles/myzip.zip", "w",zipfile.ZIP_DEFLATED)
	z.write('PREDICTORS.txt')
	z.write('RESIDUES.txt')
	z.printdir()
	z.close()

	call(["mv", "PREDICTORS.txt", "outputFiles"])
	call(["mv", "RESIDUES.txt", "outputFiles"])
		# print "++++++++++++++++++++++++++++++++++++++++++++++++++++"
	return OUTpredictors, OUTresidues